package mx.com.zara.prices.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Jorge Ortiz for Academia Microservicios - Prueba2-PruebasUnitarias on 15/07/2022
 */
class PriceResponseTest {

    PricesResponse pricesResponse;

    @BeforeEach
    void setUp() {
        pricesResponse = new PricesResponse();
    }

    @AfterEach
    void tearDown() {
        pricesResponse = null;
    }

    @Test
    void testSetIdProducto() {
        PricesResponse pricesResponse1 = new PricesResponse();
        pricesResponse1.setIdProducto(2);
        assertEquals(2, pricesResponse1.getIdProducto());
    }

    @Test
    void testGetIdProducto() {
        PricesResponse pricesResponse1 = new PricesResponse();
        pricesResponse1.setIdProducto(2);
        assertEquals(2,pricesResponse1.getIdProducto());
    }

    @Test
    void testGetIdMarca() {
        PricesResponse pricesResponse1 = new PricesResponse();
        pricesResponse1.setIdMarca(2);
        assertEquals(2,pricesResponse1.getIdMarca());
    }

    @Test
    void testSetIdMarca() {
        PricesResponse pricesRequest1 = new PricesResponse();
        pricesRequest1.setIdMarca(8);
        assertEquals(8, pricesRequest1.getIdMarca());
    }

    @Test
    void testGetTarifa() {
        PricesResponse pricesResponse1 = new PricesResponse();
        pricesResponse1.setTarifa(2);
        assertEquals(2,pricesResponse1.getTarifa());
    }

    @Test
    void testSetTarifa() {
        PricesResponse pricesResponse1 = new PricesResponse();
        pricesResponse1.setTarifa(2);
        assertEquals(2, pricesResponse1.getTarifa());
    }

    @Test
    void testGetPrecio() {
        assertNull(pricesResponse.getPrecio());
    }

    @Test
    void testSetPrecio() {
        PricesResponse pricesResponse1 = new PricesResponse();
        pricesResponse1.setPrecio(BigDecimal.valueOf(2.00));
        assertEquals(BigDecimal.valueOf(2.00), pricesResponse1.getPrecio());
    }

    @Test
    void testGetFechaInicio() {
        assertNull(pricesResponse.getFechaInicio());
    }

    @Test
    void testSetFechaInicio() {
        PricesResponse pricesRequest1 = new PricesResponse();
        pricesRequest1.setFechaInicio(new Date(2022, Calendar.JULY, 15));
        assertEquals(new Date(2022, Calendar.JULY, 15), pricesRequest1.getFechaInicio());
    }

    @Test
    void testGetFechaTermino() {
        assertNull(pricesResponse.getFechaTermino());
    }

    @Test
    void testSetFechaTermino() {
        PricesResponse pricesRequest1 = new PricesResponse();
        pricesRequest1.setFechaTermino(new Date(2022, Calendar.JULY, 15));
        assertEquals(new Date(2022, Calendar.JULY, 15), pricesRequest1.getFechaTermino());
    }

}
