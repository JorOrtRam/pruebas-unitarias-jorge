package mx.com.zara.prices.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.Null;

class PricesRequestTest {

    PricesRequest pricesRequest;

    @BeforeEach
    void setUp() {
        pricesRequest = new PricesRequest();
    }

    @AfterEach
    void tearDown() {
        pricesRequest = null;
    }

    @Test
    void testGetFechaAplicacion() {
        assertNull(pricesRequest.getFechaAplicacion());
    }

    @Test
    void testSetFechaAplicacion() {
        PricesRequest pricesRequest1 = new PricesRequest();
        pricesRequest1.setFechaAplicacion("test");
        assertEquals("test", pricesRequest1.getFechaAplicacion());
    }

    @Test
    void testGetIdProducto() {
        assertNull(pricesRequest.getIdProducto());
    }

    @Test
    void testSetIdProducto() {
        PricesRequest pricesRequest1 = new PricesRequest();
        pricesRequest1.setIdProducto(2);
        assertEquals(2, pricesRequest1.getIdProducto());
    }

    @Test
    void testGetIdMarca() {
        assertNull(pricesRequest.getIdMarca());
    }

    @Test
    void testSetIdMarca() {
        PricesRequest pricesRequest1 = new PricesRequest();
        pricesRequest1.setIdMarca(1);
        assertEquals(1, pricesRequest1.getIdMarca());
    }


}
