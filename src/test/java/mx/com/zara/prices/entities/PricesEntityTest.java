package mx.com.zara.prices.entities;

import static org.junit.jupiter.api.Assertions.*;

import mx.com.zara.prices.model.PricesRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

class PricesEntityTest {

    PricesEntity pricesEntity;

    @BeforeEach
    public void setUp() {
        pricesEntity = new PricesEntity();
    }

    @AfterEach
    public void tearDown() {
        pricesEntity = null;
    }


    @Test
    void testGetIdBrand() {
        PricesEntity pricesEntity1 = new PricesEntity();
        pricesEntity1.setIdBrand(2);
        assertEquals(2, pricesEntity1.getIdBrand());
    }

    @Test
    void testSetIdBrand() {
        assertNotNull(pricesEntity);
    }

    @Test
    void testGetPriceList() {
        PricesEntity pricesEntity1 = new PricesEntity();
        pricesEntity1.setPriceList(2);
        assertEquals(2, pricesEntity1.getPriceList());
    }

    @Test
    void testSetPriceList() {
        assertNotNull(pricesEntity);
    }

    @Test
    void testGetStartDate() {
        PricesEntity pricesEntity1 = new PricesEntity();
        Date date = new Date(2022,Calendar.JULY,15);
        pricesEntity1.setStartDate(date);
        assertEquals(date, pricesEntity1.getStartDate());
    }

    @Test
    void testSetStartDate() {
        PricesEntity pricesEntity1 = new PricesEntity();
        Date date = new Date(2022, Calendar.JULY, 15, 16, 0, 0);
        Date date1 = new Date(2022, Calendar.JULY, 15, 16, 0, 0);
        pricesEntity1.setStartDate(date);
        assertEquals(date1, pricesEntity1.getStartDate());
    }

    @Test
    void testGetEndDate() {
        PricesEntity pricesEntity1 = new PricesEntity();
        Date date = new Date(2022,Calendar.JULY,15);
        pricesEntity1.setEndDate(date);
        assertEquals(date, pricesEntity1.getEndDate());
    }

    @Test
    void testSetEndDate() {
        PricesEntity pricesEntity1 = new PricesEntity();
        Date date = new Date(2022, Calendar.JULY, 15, 16, 0, 0);
        Date date1 = new Date(2022, Calendar.JULY, 15, 16, 0, 0);
        pricesEntity1.setEndDate(date);
        assertEquals(date1, pricesEntity1.getEndDate());
    }

    @Test
    void testGetProductId() {
        PricesEntity pricesEntity1 = new PricesEntity();
        pricesEntity1.setProductId(2);
        assertEquals(2, pricesEntity1.getProductId());
    }

    @Test
    void testSetProductoId() {
        assertNotNull(pricesEntity);
    }

    @Test
    void testGetPriority() {
        PricesEntity pricesEntity1 = new PricesEntity();
        pricesEntity1.setPriority(2);
        assertEquals(2, pricesEntity1.getPriority());
    }

    @Test
    void testSetPriority() {
        assertNotNull(pricesEntity);
    }

    @Test
    void testGetPrice() {
        PricesEntity pricesEntity1 = new PricesEntity();
        pricesEntity1.setPrice(BigDecimal.valueOf(2.00));
        assertEquals(BigDecimal.valueOf(2.00), pricesEntity1.getPrice());
    }

    @Test
    void testSetPrice() {
        assertNotNull(pricesEntity);
    }

    @Test
    void testGetCurrency() {
        PricesEntity pricesEntity1 = new PricesEntity();
        pricesEntity1.setCurrency("TST");
        assertSame("TST", pricesEntity1.getCurrency());
    }

    @Test
    void testSetCurrency() {
        assertNotNull(pricesEntity);
    }
}
