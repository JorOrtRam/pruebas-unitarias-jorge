package mx.com.zara.prices.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import java.util.Calendar;

import java.util.GregorianCalendar;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import mx.com.zara.prices.entities.PricesEntity;

import mx.com.zara.prices.repository.PricesRepository;

import org.mockito.Mockito;

class PricesServiceTest {

    PricesEntity pricesEntity;

    PricesService pricesServices;

    @BeforeEach
    void setUp() {
        pricesServices = new PricesService();
        pricesServices.pricesRepository = Mockito.mock(PricesRepository.class);
        pricesEntity = new PricesEntity();
    }

    @AfterEach
    void tearDown(){
        pricesServices = null;
        pricesEntity = null;
    }

    @Test
    void getPricesByIdProductAndIdBrandAndApplicationDateTest() {
        when(pricesServices.pricesRepository.findPricesByIdProductAndBrand("", "", "")).thenReturn(pricesEntity);
        assertNotNull(pricesServices.getPricesByIdProductAndIdBrandAndApplicationDate("", "", ""));
    }

    @Test
    void isNumberTest() {
        assertEquals(true, pricesServices.isNumber("4"));
    }

    @Test
    void isNumberTestNot() {
        assertNotEquals(true, pricesServices.isNumber("s"));
    }
}
