import mx.com.zara.prices.ShopApplication;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Created by Jorge Ortiz for Academia Microservicios - Prueba2-PruebasUnitarias on 15/07/2022
 */


// Test class added ONLY to cover main() invocation not covered by application tests.
class ShopApplicationTest {
    @Disabled("Aun no se como testear las aplicaciones") //Aun no se como testear las aplicaciones
    @Test
    void main() {
        ShopApplication.main(new String[] {});
    }
}
